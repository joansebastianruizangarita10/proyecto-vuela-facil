-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-10-2021 a las 06:18:12
-- Versión del servidor: 10.4.20-MariaDB
-- Versión de PHP: 7.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vuelafacil`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agencia`
--

CREATE TABLE `agencia` (
  `IDAGENCIA` varchar(100) NOT NULL,
  `DIRECCION` varchar(100) NOT NULL,
  `TELEFONO` varchar(15) NOT NULL,
  `CIUDAD` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente_de_viajes`
--

CREATE TABLE `agente_de_viajes` (
  `IDAGENTEVIAJES` varchar(100) NOT NULL,
  `IDPERSONA` varchar(100) NOT NULL,
  `IDAGENCIA` varchar(100) NOT NULL,
  `FECHACONTRATACION` date NOT NULL,
  `PORCENTAJE_DE_COMISION` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pasajero`
--

CREATE TABLE `pasajero` (
  `IDPASAJERO` varchar(100) NOT NULL,
  `IDPERSONA` varchar(100) NOT NULL,
  `TIPO_PASAJERO` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `IDPERSONA` varchar(11) NOT NULL,
  `NOMBRE` varchar(100) NOT NULL,
  `APELLIDO` varchar(100) NOT NULL,
  `CIUDAD` varchar(100) NOT NULL,
  `TELEFONO` varchar(100) NOT NULL,
  `DIRECCION` varchar(100) NOT NULL,
  `GENERO` varchar(100) NOT NULL,
  `EDAD` int(11) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `USUARIO` varchar(100) NOT NULL,
  `CONTRASENA` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ruta`
--

CREATE TABLE `ruta` (
  `IDRUTA` varchar(100) NOT NULL,
  `IDPERSONA` varchar(100) NOT NULL,
  `COSTO` int(11) NOT NULL,
  `TIEMPO_ESTIMADO` float NOT NULL,
  `CIUDAD_ORIGEN` varchar(100) NOT NULL,
  `CIUDAD_DESTINO` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vuelo`
--

CREATE TABLE `vuelo` (
  `NUMEROVUELO` varchar(100) NOT NULL,
  `IDRUTA` varchar(100) NOT NULL,
  `ORIGEN` varchar(100) NOT NULL,
  `DESTINO` varchar(100) NOT NULL,
  `FECHASALIDA` datetime NOT NULL,
  `FECHALLEGADA` datetime NOT NULL,
  `TIEMPOESTIMADO` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `agencia`
--
ALTER TABLE `agencia`
  ADD PRIMARY KEY (`IDAGENCIA`);

--
-- Indices de la tabla `agente_de_viajes`
--
ALTER TABLE `agente_de_viajes`
  ADD PRIMARY KEY (`IDAGENTEVIAJES`),
  ADD KEY `FK_Persona_Agente_Viajes` (`IDPERSONA`),
  ADD KEY `FK_Agencia_Agente_Viajes` (`IDAGENCIA`);

--
-- Indices de la tabla `pasajero`
--
ALTER TABLE `pasajero`
  ADD PRIMARY KEY (`IDPASAJERO`),
  ADD KEY `FK_PERSONA_PASAJERO` (`IDPERSONA`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`IDPERSONA`);

--
-- Indices de la tabla `ruta`
--
ALTER TABLE `ruta`
  ADD PRIMARY KEY (`IDRUTA`),
  ADD KEY `FK_RUTA_PERSONA` (`IDPERSONA`);

--
-- Indices de la tabla `vuelo`
--
ALTER TABLE `vuelo`
  ADD PRIMARY KEY (`NUMEROVUELO`),
  ADD KEY `FK_RUTA_VUELO` (`IDRUTA`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `agente_de_viajes`
--
ALTER TABLE `agente_de_viajes`
  ADD CONSTRAINT `FK_Agencia_Agente_Viajes` FOREIGN KEY (`IDAGENCIA`) REFERENCES `agencia` (`IDAGENCIA`),
  ADD CONSTRAINT `FK_Persona_Agente_Viajes` FOREIGN KEY (`IDPERSONA`) REFERENCES `persona` (`IDPERSONA`);

--
-- Filtros para la tabla `pasajero`
--
ALTER TABLE `pasajero`
  ADD CONSTRAINT `FK_PERSONA_PASAJERO` FOREIGN KEY (`IDPERSONA`) REFERENCES `persona` (`IDPERSONA`);

--
-- Filtros para la tabla `ruta`
--
ALTER TABLE `ruta`
  ADD CONSTRAINT `FK_RUTA_PERSONA` FOREIGN KEY (`IDPERSONA`) REFERENCES `persona` (`IDPERSONA`);

--
-- Filtros para la tabla `vuelo`
--
ALTER TABLE `vuelo`
  ADD CONSTRAINT `FK_RUTA_VUELO` FOREIGN KEY (`IDRUTA`) REFERENCES `ruta` (`IDRUTA`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
