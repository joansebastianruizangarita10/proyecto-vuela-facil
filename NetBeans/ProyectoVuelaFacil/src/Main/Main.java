/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;
 import Modelo.*;
import Vista.*;
import Controlador.*;

/**
 *
 * @author Joan Ruiz
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //miConexion.agregarPasajero("2", "Luis", "Ramirez", "Floridablanca", "3012547556", "Calle 100#45-85", "Masculino", 25, "luisramirez@gmail.com", "luisramirez", "luis123", "2", "Ejecutivo");
//        miConexion.actualizarPasajero("1", "Luis", "Ramirez", "Floridablanca", "3012547556", "Calle 100#45-85", "Masculino", 25, "luisramirez@gmail.com", "luisramirez", "luis123", "2", "Ejecutivo");

//        conexionSQL miConexion = new conexionSQL();
//        miConexion.conexion();
//        CrudPasajero miVista = new CrudPasajero();
//        Controlador miControlador = new Controlador(miVista,miConexion);
//        miControlador.iniciarVista();
//        miVista.setVisible(true);
//        String a=miConexion.logueo("luisramirez", "luis123");
//        System.out.println(a);
        

        conexionSQL miConexion = new conexionSQL();
        miConexion.conexion();
        Logueo miVista = new Logueo();
        ControladorLogueo miControlador = new ControladorLogueo(miVista,miConexion);
        miControlador.iniciarVista();
        miVista.setVisible(true);
    }
    
}
