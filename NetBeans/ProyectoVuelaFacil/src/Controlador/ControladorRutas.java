/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Vista.*;
import Modelo.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import javax.swing.*;
/**
 *
 * @author Joan Ruiz
 */
public class ControladorRutas {
    private RutasDisponibles vista;
    private conexionSQL conexion;
    
    public ControladorRutas(RutasDisponibles vista, conexionSQL conexion){
        this.vista=vista;
        this.conexion=conexion;
           
    }
    
    public void iniciarVista(){
        
        vista.setLocationRelativeTo(null);
    }
}
