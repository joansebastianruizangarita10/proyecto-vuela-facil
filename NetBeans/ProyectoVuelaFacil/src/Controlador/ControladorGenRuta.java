/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Vista.*;
import Modelo.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import javax.swing.*;
/**
 *
 * @author Joan Ruiz
 */
public class ControladorGenRuta implements ActionListener {
    
    private GenRuta vista;
    private conexionSQL conexion;
    
    public ControladorGenRuta(GenRuta vista, conexionSQL conexion){
        this.vista=vista;
        this.conexion=conexion;
        this.vista.buscar.addActionListener(this);    
    }
    
    public void iniciarVista(){
        
        vista.setLocationRelativeTo(null);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        JButton accion = (JButton) e.getSource();
        if(accion.equals(vista.buscar)){
            vista.dispose();
            conexionSQL conexionRutas = new conexionSQL();
                conexionRutas.conexion();
                RutasDisponibles vistaRutasDisponibles = new RutasDisponibles();
                ControladorRutas controladorRutas = new ControladorRutas(vistaRutasDisponibles,conexionRutas);
                controladorRutas.iniciarVista();
                vistaRutasDisponibles.setVisible(true);
        }
}
}
