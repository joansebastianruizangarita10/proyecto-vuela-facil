/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Vista.*;
import Modelo.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author Joan Ruiz
 */
public class ControladorCrudPasajero implements ActionListener {
    private CrudPasajero vista;
    private conexionSQL conexion;
    
    
    public ControladorCrudPasajero(CrudPasajero vista, conexionSQL conexion){
        this.vista=vista;
        this.conexion=conexion;
        this.vista.agregar.addActionListener(this);
        this.vista.actualizar.addActionListener(this);
        this.vista.eliminar.addActionListener(this);
        this.vista.consultar.addActionListener(this);       
    }
    
    public void iniciarVista(){
        
        vista.setLocationRelativeTo(null);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        JButton accion = (JButton) e.getSource();
        if(accion.equals(vista.agregar)){
            System.out.println("Agregado");
            String idPersona = vista.idPersona.getText();
            String idPasajero = vista.idPasajero.getText();
            String nombre = vista.nombre.getText();
            String apellido = vista.apellido.getText();
            String ciudad = vista.ciudad.getText();
            String telefono = vista.telefono.getText();
            String direccion = vista.direccion.getText();
            String genero = (String)vista.genero.getSelectedItem();
            int edad = Integer.valueOf(vista.edad.getText());
            String email = vista.email.getText();
            String tipoPasajero = vista.tipoPasajero.getText();
            String usuario = vista.usuario.getText();
            String contrasena = vista.contrasena.getText();
            
            conexion.agregarPasajero(idPersona, nombre, apellido, ciudad, telefono, direccion, 
                    genero, edad, email, usuario, contrasena, idPasajero, tipoPasajero);
        }
        else if(accion.equals(vista.eliminar)){
            String idPersonaEliminar = vista.idPersonaEliminar.getText();
            conexion.eliminarPasajero(idPersonaEliminar);
            System.out.println("Eliminado");
        }
        else if(accion.equals(vista.actualizar)){
            String idPersona = vista.idPersona.getText();
            String idPasajero = vista.idPasajero.getText();
            String nombre = vista.nombre.getText();
            String apellido = vista.apellido.getText();
            String ciudad = vista.ciudad.getText();
            String telefono = vista.telefono.getText();
            String direccion = vista.direccion.getText();
            String genero = (String)vista.genero.getSelectedItem();
            int edad = Integer.valueOf(vista.edad.getText());
            String email = vista.email.getText();
            String tipoPasajero = vista.tipoPasajero.getText();
            String usuario = vista.usuario.getText();
            String contrasena = vista.contrasena.getText();
            
            conexion.actualizarPasajero(idPersona, nombre, apellido, ciudad, telefono, 
                    direccion, genero, edad, email, usuario, contrasena, idPasajero, tipoPasajero);
            System.out.println("Actualizado");
        }
        else if(accion.equals(vista.consultar)){
            
            System.out.println("Consultado");
            Object[] nombresColumnas = {"Id Persona","Id Pasajero","Tipo de pasajero","Nombre",
                "Apellido", "Ciudad", "Teléfono", "Dirección", "Género", "Edad", "Email", 
                "Usuario", "Contraseña"};
            DefaultTableModel modelo = new DefaultTableModel();
            modelo.setColumnIdentifiers(nombresColumnas);
            vista.consultaPasajeros.setModel(modelo);
            Object [][] filaDatos = new Object[1][13];
            LinkedList<Pasajero> lista = new LinkedList<Pasajero>();
            lista=conexion.leerPasajeros();
            
            for(Pasajero pasajero:lista){
                filaDatos[0][0]=pasajero.getIdPersona();
                filaDatos[0][1]=pasajero.getIdPasajero();
                filaDatos[0][2]=pasajero.getTipoPasajero();
                filaDatos[0][3]=pasajero.getNombre();
                filaDatos[0][4]=pasajero.getApellido();
                filaDatos[0][5]=pasajero.getCiudad();
                filaDatos[0][6]=pasajero.getTelefono();
                filaDatos[0][7]=pasajero.getDireccion();
                filaDatos[0][8]=pasajero.getGenero();
                filaDatos[0][9]=pasajero.getEdad();
                filaDatos[0][10]=pasajero.getEmail();
                filaDatos[0][11]=pasajero.getUsuario();
                filaDatos[0][12]=pasajero.getContrasena();
                modelo.addRow(filaDatos[0]);
            }
        }
    }
}
