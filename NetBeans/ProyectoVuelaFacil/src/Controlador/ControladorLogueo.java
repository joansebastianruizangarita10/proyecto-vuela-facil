/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Vista.*;
import Modelo.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import javax.swing.*;
/**
 *
 * @author Joan Ruiz
 */
public class ControladorLogueo implements ActionListener {
    private Logueo vista;
    private conexionSQL conexion;
    
    
    public ControladorLogueo(Logueo vista, conexionSQL conexion){
        this.vista=vista;
        this.conexion=conexion;
        this.vista.ingresar.addActionListener(this);    
    }
    
    public void iniciarVista(){
        
        vista.setLocationRelativeTo(null);
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        JButton accion = (JButton) e.getSource();
        if(accion.equals(vista.ingresar)){
            System.out.println("Ingresado");
            String usuarioLogueo = vista.usuarioLogueo.getText();
            String contrasenaLogueo = vista.contrasenaLogueo.getText();
            
            String decision=conexion.logueo(usuarioLogueo,contrasenaLogueo);
            
            if (decision.equals("Agente de viajes")){
                vista.dispose(); //cierra la ventana anterior
                conexionSQL conexionCrudPasajero = new conexionSQL();
                conexionCrudPasajero.conexion();
                CrudPasajero vistaCrudPasajero = new CrudPasajero();
                ControladorCrudPasajero controladorCrudPasajero = new ControladorCrudPasajero(vistaCrudPasajero,conexionCrudPasajero);
                controladorCrudPasajero.iniciarVista();
                vistaCrudPasajero.setVisible(true);
            }
            else if (decision.equals("Pasajero")){
                vista.dispose();
                conexionSQL conexionGenRuta = new conexionSQL();
                conexionGenRuta.conexion();
                GenRuta vistaGenRuta = new GenRuta();
                ControladorGenRuta controladorGenRuta = new ControladorGenRuta(vistaGenRuta,conexionGenRuta);
                controladorGenRuta.iniciarVista();
                vistaGenRuta.setVisible(true);
            }

        }
}
}
