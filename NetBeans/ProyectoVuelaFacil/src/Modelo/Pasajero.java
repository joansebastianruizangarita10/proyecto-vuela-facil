/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author TAD
 */
public class Pasajero extends Persona{
    
    private String idPasajero;
    private String tipoPasajero;

    public Pasajero(String idPasajero, String tipoPasajero, String idPersona, String nombre, String apellido, String ciudad, String telefono, String direccion, String genero, int edad, String email, String usuario, String contrasena) {
        super(idPersona, nombre, apellido, ciudad, telefono, direccion, genero, edad, email, usuario, contrasena);
        this.idPasajero = idPasajero;
        this.tipoPasajero = tipoPasajero;
    }

    public String getIdPasajero() {
        return idPasajero;
    }

    public String getTipoPasajero() {
        return tipoPasajero;
    }

    public void setIdPasajero(String idPasajero) {
        this.idPasajero = idPasajero;
    }

    public void setTipoPasajero(String tipoPasajero) {
        this.tipoPasajero = tipoPasajero;
    }

    
    
    
    
}
