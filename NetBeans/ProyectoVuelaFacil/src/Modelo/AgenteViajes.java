/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author TAD
 */
public class AgenteViajes extends Persona {
    private String idAgenteViajes;
    private String idAgencia;
    private String fechaContratacion;
    private double porcentajeComision;

    public AgenteViajes(String idAgenteViajes, String idAgencia, String fechaContratacion, double porcentajeComision, String idPersona, String nombre, String apellido, String ciudad, String telefono, String direccion, String genero, int edad, String email, String usuario, String contrasena) {
        super(idPersona, nombre, apellido, ciudad, telefono, direccion, genero, edad, email, usuario, contrasena);
        this.idAgenteViajes = idAgenteViajes;
        this.idAgencia = idAgencia;
        this.fechaContratacion = fechaContratacion;
        this.porcentajeComision = porcentajeComision;
    }

    public String getIdAgenteViajes() {
        return idAgenteViajes;
    }

    public String getIdAgencia() {
        return idAgencia;
    }

    public String getFechaContratacion() {
        return fechaContratacion;
    }

    public double getPorcentajeComision() {
        return porcentajeComision;
    }

    public void setIdAgenteViajes(String idAgenteViajes) {
        this.idAgenteViajes = idAgenteViajes;
    }

    public void setIdAgencia(String idAgencia) {
        this.idAgencia = idAgencia;
    }

    public void setFechaContratacion(String fechaContratacion) {
        this.fechaContratacion = fechaContratacion;
    }

    public void setPorcentajeComision(double porcentajeComision) {
        this.porcentajeComision = porcentajeComision;
    }

    
    
  
    
}
