/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Joan Ruiz
 */
public class Agencia {
    private int idAgencia;
    private String direccion;
    private double telefono;
    private String ciudad;

    public Agencia(int idAgencia, String direccion, double telefono, String ciudad) {
        this.idAgencia = idAgencia;
        this.direccion = direccion;
        this.telefono = telefono;
        this.ciudad = ciudad;
    }

    public int getIdAgencia() {
        return idAgencia;
    }

    public String getDireccion() {
        return direccion;
    }

    public double getTelefono() {
        return telefono;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setIdAgencia(int idAgencia) {
        this.idAgencia = idAgencia;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setTelefono(double telefono) {
        this.telefono = telefono;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }
    
    
}
