/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import com.mysql.jdbc.Connection;
import java.sql.*;
import java.sql.DriverManager;
import java.util.LinkedList;
import javax.swing.JOptionPane;

/**
 *
 * @author TAD
 */
public class conexionSQL {
    
    Connection conectar=null;
    
    public Connection conexion(){
    
    try{
    Class.forName("com.mysql.jdbc.Driver");
    conectar=(Connection) DriverManager.getConnection("jdbc:mysql://localhost/vuelafacil","root","");
    
    
    //JOptionPane.showMessageDialog(null,"conexion exitosa");
    }
    
    catch(Exception e)
    {
       JOptionPane.showMessageDialog(null,"error conexion"+e.getMessage());
        
    }
        return conectar;
    }
    
    public void ejecutarQuery(String query){
        
        try{
            Statement stmt = conectar.createStatement();
            stmt.execute(query);
           
        } catch(SQLException e){
            System.out.println(e.getMessage());
            
        }
        
    }
    
    public ResultSet ejecutarQueryLectura(String query){
        ResultSet rs = null;
        try{
            Statement stmt = conectar.createStatement();
            rs = stmt.executeQuery(query);
            
        } catch(SQLException e){
            System.out.println(e.getMessage());
           
        }
        return rs;
    }
    
    public String logueo(String usuario,String contrasena){
        String query = "select  USUARIO, CONTRASENA\n"
                +"FROM persona\n"
                +"WHERE USUARIO=\""+usuario+"\";";
        
        String usuarioAprobrar="";
        String contrasenaAprobar="";
        String decision="";
        
        try{
            ResultSet rs = ejecutarQueryLectura(query);
            while(rs.next()){
                
                usuarioAprobrar = rs.getString("USUARIO");
                contrasenaAprobar = rs.getString("CONTRASENA");
                
                if (usuarioAprobrar.equals(usuario) && contrasenaAprobar.equals(contrasena)){
//                    System.out.println("Logueo correcto");
                    
                    String query2 = "SELECT IDPASAJERO FROM pasajero INNER JOIN persona "
                            + "ON pasajero.IDPERSONA = persona.IDPERSONA and "
                            + "usuario=\""+usuario+"\";";

                    String idPasajeroAprobar="";
                    decision="Agente de viajes";

                    try{
                        ResultSet rs2 = ejecutarQueryLectura(query2);
                        while(rs2.next()){

                            idPasajeroAprobar = rs2.getString("IDPASAJERO");

                            if (!idPasajeroAprobar.equals("")){
                                decision="Pasajero";


                            }else{
                                decision="Agente de viajes";
                            }
                            }
                    } catch(SQLException e){
                        System.out.println(e.getMessage());
                    }
                    
                }else{
                    decision="Datos incorrectos";
                }
                }
        } catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return decision;
    }
    
    public void agregarPasajero(String idPersona,String nombre,String apellido,String ciudad,String telefono,
            String direccion,String genero,int edad,String email,String usuario,String contrasena,
            String idPasajero,String tipoPasajero){
        String query = "INSERT INTO persona (IDPERSONA,NOMBRE,APELLIDO,CIUDAD,TELEFONO,DIRECCION,GENERO,"
                + "EDAD,EMAIL,USUARIO,CONTRASENA)"
                + "VALUES ("+"\""+idPersona+"\","+"\""+nombre+"\","+"\""+apellido+"\","+"\""+ciudad+"\","
                +"\""+telefono+"\","+"\""+direccion+"\","+"\""+genero+"\","+edad+",\""+email+"\","
                +"\""+usuario+"\","+"\""+contrasena+"\");";
        String query2 = "INSERT INTO pasajero (IDPASAJERO,IDPERSONA,TIPO_PASAJERO)"
                + "VALUES ("+"\""+idPasajero+"\","+"\""+idPersona+"\","+"\""+tipoPasajero+"\");";
                
      ejecutarQuery(query);
      ejecutarQuery(query2);
    }
    
    public void eliminarPasajero (String idPersona){
        String query = "DELETE FROM pasajero WHERE IDPERSONA="+"\""+idPersona+"\";";
        String query2 = "DELETE FROM persona WHERE IDPERSONA="+"\""+idPersona+"\";";
       ejecutarQuery(query);
       ejecutarQuery(query2);
    }
    
    public void actualizarPasajero(String idPersona,String nombre,String apellido,String ciudad,String telefono,
            String direccion,String genero,int edad,String email,String usuario,String contrasena,
            String idPasajero,String tipoPasajero){
        
        String query = "UPDATE persona SET NOMBRE=\""+nombre+"\""+", APELLIDO=\""+apellido+"\","
                +"CIUDAD=\""+ciudad+"\","+"TELEFONO=\""+telefono+"\","
                +"DIRECCION=\""+direccion+"\","+"GENERO=\""+genero+"\", EDAD="+edad
                +", EMAIL=\""+email+"\","+"USUARIO= \""+usuario+"\","
                +"CONTRASENA= \""+contrasena+"\" WHERE IDPERSONA="+idPersona+";";
        
        String query2 = "UPDATE pasajero SET TIPO_PASAJERO=\""+tipoPasajero+"\""
                +"WHERE IDPERSONA="+idPersona+";";
        ejecutarQuery(query);
        ejecutarQuery(query2);
    }
    
    public LinkedList leerPasajeros(){
        String query = "select  pasajero.IDPERSONA,pasajero.IDPASAJERO, TIPO_PASAJERO, "
                + "NOMBRE, APELLIDO, CIUDAD, TELEFONO, DIRECCION, GENERO, EDAD, EMAIL,"
                + "USUARIO,CONTRASENA\n"
                +"FROM persona, pasajero\n"
                +"WHERE pasajero.IDPERSONA=persona.IDPERSONA;";
        
        String idPersona="";
        String idPasajero="";
        String tipoPasajero="";
        String nombre="";
        String apellido="";
        String ciudad="";
        String telefono="";
        String direccion="";
        String genero="";
        String edad="";
        String email="";
        String usuario="";
        String contrasena="";
        
        LinkedList<Pasajero> listaPasajeros = new LinkedList<Pasajero>();
        
        try{
            ResultSet rs = ejecutarQueryLectura(query);
            while(rs.next()){
                idPersona=rs.getString("IDPERSONA");
                idPasajero = rs.getString("IDPASAJERO");
                tipoPasajero = rs.getString("TIPO_PASAJERO");
                nombre = rs.getString("NOMBRE");
                apellido = rs.getString("APELLIDO");
                ciudad = rs.getString("CIUDAD");
                telefono = rs.getString("TELEFONO");
                direccion = rs.getString("DIRECCION");
                genero = rs.getString("GENERO");
                edad = rs.getString("EDAD");
                email = rs.getString("EMAIL");
                usuario = rs.getString("USUARIO");
                contrasena = rs.getString("CONTRASENA");
                
                Pasajero pasajeroAañadir = new Pasajero(idPasajero, tipoPasajero, idPersona, nombre,
                        apellido, ciudad, telefono, direccion, genero,Integer.valueOf(edad), email,
                        usuario, contrasena);
                        
                listaPasajeros.add(pasajeroAañadir);
            }
        } catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return listaPasajeros;
        
    }
}
