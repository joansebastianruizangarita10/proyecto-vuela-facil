/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Joan Ruiz
 */
public class Ruta {
    
    private int idRuta;
    private int idPersona;
    private double costo;
    private double tiempoEstimado;
    private String ciudadOrigen;
    private String ciudadDestino;

    public Ruta(int idRuta, int idPersona, double costo, double tiempoEstimado, String ciudadOrigen, String ciudadDestino) {
        this.idRuta = idRuta;
        this.idPersona = idPersona;
        this.costo = costo;
        this.tiempoEstimado = tiempoEstimado;
        this.ciudadOrigen = ciudadOrigen;
        this.ciudadDestino = ciudadDestino;
    }

    public int getIdRuta() {
        return idRuta;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public double getCosto() {
        return costo;
    }

    public double getTiempoEstimado() {
        return tiempoEstimado;
    }

    public String getCiudadOrigen() {
        return ciudadOrigen;
    }

    public String getCiudadDestino() {
        return ciudadDestino;
    }

    public void setIdRuta(int idRuta) {
        this.idRuta = idRuta;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }

    public void setTiempoEstimado(double tiempoEstimado) {
        this.tiempoEstimado = tiempoEstimado;
    }

    public void setCiudadOrigen(String ciudadOrigen) {
        this.ciudadOrigen = ciudadOrigen;
    }

    public void setCiudadDestino(String ciudadDestino) {
        this.ciudadDestino = ciudadDestino;
    }

    
    
}
